#!/usr/bin/env python
# -*- coding: utf-8 -*-

import json


def collatz(n):
    """Calculates the number chains from the Collatz Conjecture,
    down from n to 1.
    Returns a (reversed) list, starting at 1 and ending in n.
    """
    l = [n]
    while l[-1] > 1:
        if n % 2:  # odd
            n = n * 3 + 1
        else:  # even
            n = n / 2
        l.append(n)

    l.reverse()
    return l

def trie(arrays):
    """Takes a list of lists of numbers and creates a trie from them.
    Returns the trie as a dictionary.
    """
    root = dict()
    for array in arrays:
        cur_dict = root
        for n in array:
            cur_dict = cur_dict.setdefault(n, {})
    return root


def pprint(iterable):
    print(
        json.dumps(
            iterable,
            sort_keys=True,
            indent=2
        )
    )

def traverse(t):
    for k, v in t.iteritems():
        print(k)
        traverse(v)

if __name__ == "__main__":
    t = trie([collatz(i) for i in range(3, 10000)])
    pprint(t)
